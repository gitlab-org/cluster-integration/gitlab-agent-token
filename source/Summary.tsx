import React, {FC} from 'react';
import {Text, Box} from 'ink';

const Summary: 
	FC<{gitlabUrl: string, projectPath: string, agentName: string, accessToken: string}> = 
	({gitlabUrl, projectPath, agentName, accessToken}) => {
	return (<Box flexDirection="column" borderColor="blue" borderStyle="round" paddingX={1}>
		<Box marginRight={1}>
			<Text>Your GitLab url is at: {gitlabUrl}</Text>
		</Box>
		<Box marginRight={1}>
			<Text>Private Access Token is {accessToken ? <Text color="green">set</Text> : <Text color="yellow">unset</Text>}</Text>
		</Box>
		<Box marginRight={1}>
			<Text>The project path is: {projectPath}</Text>
		</Box>
		<Box marginRight={1}>
			<Text>The agent name is: {agentName}</Text>
		</Box>
	</Box>)
}

export default Summary