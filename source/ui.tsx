import React, {FC, useState} from 'react';
import {Box, Spacer} from 'ink';
import Setup from './Setup';
import Summary from './Summary';
import GetToken from './GetToken';
import Joi from 'joi';

const App: FC = () => {
	const [gitlabUrl, setGitlabUrl] = useState('')
	const [accessToken, setAccessToken] = useState('')
	const [projectPath, setProjectPath] = useState('')
	const [agentName, setAgentName] = useState('')
	const [step, setStep] = useState('setGitlabUrl')

	const moveToStep = (step: string, setter: (value: string) => void) => {
		return function(value: string) {
			setter(value)
			setStep(step)
		}
	}

	let SetupEl: JSX.Element | null = null
	let schema
	switch(step) {
		case 'setGitlabUrl':
			schema = Joi.string().trim().required().uri({scheme: ['http', 'https']});
			SetupEl = <Setup schema={schema} title='GitLab URL' placeholder='The URL of your GitLab instance' defaultValue="https://gitlab.com" onSubmit={moveToStep('setAccessToken', setGitlabUrl)} />
			break
		case 'setAccessToken':
			schema = Joi.string().trim().required().pattern(/^[0-9a-zA-Z_\-\.]+$/);
			SetupEl = <Setup schema={schema} title='Private Access token' helpText={`A private access token with api rights is needed to register the agent. You can create a token under your GitLab profile: ${gitlabUrl}/-/profile/personal_access_tokens`} hidden placeholder='The scripts authenticates with a Private Access Token' defaultValue="" onSubmit={moveToStep('setProjectPath', setAccessToken)} />
			break
		case 'setProjectPath':
			schema = Joi.string().trim().required().pattern(/^[0-9a-zA-Z_\-\.\/]+$/);
			SetupEl = <Setup schema={schema} title='Agent configuration project path' placeholder='The configuration project path under gitlab' defaultValue='' onSubmit={moveToStep('setAgentName', setProjectPath)} />
			break
		case 'setAgentName':
			schema = Joi.string().trim().required().pattern(/^[0-9a-zA-Z_\-\.]+$/);
			SetupEl = <Setup schema={schema} title='Agent name' placeholder='The name of your agent' defaultValue='' onSubmit={moveToStep('getToken', setAgentName)} />
			break
		case 'getToken':
			SetupEl = <GetToken gitlabUrl={gitlabUrl} accessToken={accessToken} projectPath={projectPath} agentName={agentName} />
			break
	}

	return (
		<Box flexDirection="column" height={18}>
			<Summary gitlabUrl={gitlabUrl} projectPath={projectPath} agentName={agentName} accessToken={accessToken} />
			<Spacer/>
			<Box marginRight={1}>
				{SetupEl}
			</Box>
		</Box>
	)
}

module.exports = App;
export default App;
