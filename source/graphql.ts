import { ApolloClient, InMemoryCache, gql, HttpLink, NormalizedCacheObject } from '@apollo/client';
const fetch = require('node-fetch')

async function checkIfAgentExists(client: ApolloClient<NormalizedCacheObject>, projectPath: string, agentName: string): Promise<string|null> {
	let query = gql`query {
		project (
		  fullPath: "${projectPath}"
		) {
			fullPath
			clusterAgents {
			  nodes {
				name
				id
			  }
			}
		}
	  }`

	  const resp = await client.query({query})
	  if(!resp.data?.project) {
		  throw new Error(`Project \`${projectPath}\` does not exist. Create it first!`)
	  } if(resp.data?.project.clusterAgents.nodes.filter((a: {name: string}) => a.name == agentName)) {
		  return resp.data.project.clusterAgents.id
	  } else {
		  return null
	  }
}

async function createClusterAgent(client: ApolloClient<NormalizedCacheObject>, projectPath: string, agentName: string) : Promise<string> {
	let mutation = gql`
	mutation createAgent {
		createClusterAgent(input: { projectPath: "${projectPath}", name: "${agentName}" }) {
		  clusterAgent {
			id
			name
		  }
		  errors
		}
	}`
	
	const resp = await client.mutate({mutation, variables: null})
	if(resp.data?.createClusterAgent?.clusterAgent?.id) {
		return resp.data?.createClusterAgent.clusterAgent.id
	}
	throw new Error(resp.data.createClusterAgent.errors[0])
}

async function createToken(client: ApolloClient<NormalizedCacheObject>, agentId: string): Promise<string> {
	let mutation = gql`
	mutation createToken {
		clusterAgentTokenCreate(input: { clusterAgentId: "${agentId}" }) {
		  secret
		  token {
			createdAt
			id
		  }
		  errors
		}
	}`
	
	const resp = await client.mutate({mutation, variables: null})
	if(resp.data?.clusterAgentTokenCreate?.secret) {
		return resp.data.clusterAgentTokenCreate.secret
	}
	throw new Error(resp.data.clusterAgentTokenCreate.errors[0])
}

export async function registerAgentAndGetToken(gitlabUrl: string, accessToken: string, projectPath: string, agentName: string): Promise<string> {
    const client = new ApolloClient({
		uri: `${gitlabUrl}`,
		link: new HttpLink({ 
			uri: `${gitlabUrl}/api/graphql`, 
			headers: {
				'authorization': `Bearer ${accessToken}`
			},
			fetch }),
		cache: new InMemoryCache()
	});
	  
	let agentId = await checkIfAgentExists(client, projectPath, agentName)
	if(!agentId) {
		agentId = await createClusterAgent(client, projectPath, agentName)
	}
    return createToken(client, agentId)
}