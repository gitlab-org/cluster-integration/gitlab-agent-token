import React, {FC, useEffect, useState} from 'react';
import {Text, Box} from 'ink';
import TextInput from 'ink-text-input';
import { StringSchema } from 'joi';

const Setup: 
	FC<{title: string, placeholder: string, defaultValue: string, helpText?: string, onSubmit: (value: string) => void, hidden?: boolean, schema: StringSchema}> = 
	({title, placeholder, defaultValue, onSubmit, schema, helpText='', hidden=false}) => {

		const [text, setText] = useState('')
		const [error, setError] = useState('')

		useEffect(() => {
			setText('')
		}, [title])
		useEffect(() => {
			setError('')
		}, [text])

		const submitInput = (origValue: string) => {
            origValue = origValue.trim()
			if(origValue.length == 0 && defaultValue.length > 0) {
				origValue = defaultValue
            }
            const {error, value} = schema.validate(origValue)
			if (error) {
				return setError(error.message)
			}
			onSubmit(value)
		}
		
		return (
			<Box flexDirection='column'>
				{error
				? (
					<Box borderStyle="single" borderColor="red">
						<Text color="red">{error}</Text>
					</Box>
				)
				: null}
				<Box marginRight={2}>
                    <Text>{title}{defaultValue ? ` [${defaultValue}]` : ''}: </Text>
					<TextInput mask={hidden ? '*' : void 0} value={text} onChange={setText} placeholder={placeholder} onSubmit={submitInput} />
				</Box>
                <Box>
                    <Text color="grey">{helpText}</Text>
                </Box>
			</Box>
		)
}

export default Setup