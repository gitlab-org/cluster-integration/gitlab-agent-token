#!/usr/bin/env node
import React from 'react';
import {render} from 'ink';
 import meow from 'meow';
import App from './ui';

 const cli = meow(`
 	Usage
 	  $ gitlab-agent-token
 `, {
	 flags: {}
 })

render(<App {...cli.flags} />);
