export declare function registerAgentAndGetToken(gitlabUrl: string, accessToken: string, projectPath: string, agentName: string): Promise<string>;
