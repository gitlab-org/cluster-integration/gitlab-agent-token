import { FC } from 'react';
declare const GetToken: FC<{
    gitlabUrl: string;
    projectPath: string;
    agentName: string;
    accessToken: string;
}>;
export default GetToken;
