"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importStar(require("react"));
const ink_1 = require("ink");
const Setup_1 = __importDefault(require("./Setup"));
const Summary_1 = __importDefault(require("./Summary"));
const GetToken_1 = __importDefault(require("./GetToken"));
const joi_1 = __importDefault(require("joi"));
const App = () => {
    const [gitlabUrl, setGitlabUrl] = react_1.useState('');
    const [accessToken, setAccessToken] = react_1.useState('');
    const [projectPath, setProjectPath] = react_1.useState('');
    const [agentName, setAgentName] = react_1.useState('');
    const [step, setStep] = react_1.useState('setGitlabUrl');
    const moveToStep = (step, setter) => {
        return function (value) {
            setter(value);
            setStep(step);
        };
    };
    let SetupEl = null;
    let schema;
    switch (step) {
        case 'setGitlabUrl':
            schema = joi_1.default.string().trim().required().uri({ scheme: ['http', 'https'] });
            SetupEl = react_1.default.createElement(Setup_1.default, { schema: schema, title: 'GitLab URL', placeholder: 'The URL of your GitLab instance', defaultValue: "https://gitlab.com", onSubmit: moveToStep('setAccessToken', setGitlabUrl) });
            break;
        case 'setAccessToken':
            schema = joi_1.default.string().trim().required().pattern(/^[0-9a-zA-Z_\-\.]+$/);
            SetupEl = react_1.default.createElement(Setup_1.default, { schema: schema, title: 'Private Access token', helpText: `A private access token with api rights is needed to register the agent. You can create a token under your GitLab profile: ${gitlabUrl}/-/profile/personal_access_tokens`, hidden: true, placeholder: 'The scripts authenticates with a Private Access Token', defaultValue: "", onSubmit: moveToStep('setProjectPath', setAccessToken) });
            break;
        case 'setProjectPath':
            schema = joi_1.default.string().trim().required().pattern(/^[0-9a-zA-Z_\-\.\/]+$/);
            SetupEl = react_1.default.createElement(Setup_1.default, { schema: schema, title: 'Agent configuration project path', placeholder: 'The configuration project path under gitlab', defaultValue: '', onSubmit: moveToStep('setAgentName', setProjectPath) });
            break;
        case 'setAgentName':
            schema = joi_1.default.string().trim().required().pattern(/^[0-9a-zA-Z_\-\.]+$/);
            SetupEl = react_1.default.createElement(Setup_1.default, { schema: schema, title: 'Agent name', placeholder: 'The name of your agent', defaultValue: '', onSubmit: moveToStep('getToken', setAgentName) });
            break;
        case 'getToken':
            SetupEl = react_1.default.createElement(GetToken_1.default, { gitlabUrl: gitlabUrl, accessToken: accessToken, projectPath: projectPath, agentName: agentName });
            break;
    }
    return (react_1.default.createElement(ink_1.Box, { flexDirection: "column", height: 15 },
        react_1.default.createElement(Summary_1.default, { gitlabUrl: gitlabUrl, projectPath: projectPath, agentName: agentName, accessToken: accessToken }),
        react_1.default.createElement(ink_1.Spacer, null),
        react_1.default.createElement(ink_1.Box, { marginRight: 1 }, SetupEl)));
};
module.exports = App;
exports.default = App;
