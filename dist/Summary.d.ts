import { FC } from 'react';
declare const Summary: FC<{
    gitlabUrl: string;
    projectPath: string;
    agentName: string;
    accessToken: string;
}>;
export default Summary;
