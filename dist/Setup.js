"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importStar(require("react"));
const ink_1 = require("ink");
const ink_text_input_1 = __importDefault(require("ink-text-input"));
const Setup = ({ title, placeholder, defaultValue, onSubmit, schema, helpText = '', hidden = false }) => {
    const [text, setText] = react_1.useState('');
    const [error, setError] = react_1.useState('');
    react_1.useEffect(() => {
        setText('');
    }, [title]);
    react_1.useEffect(() => {
        setError('');
    }, [text]);
    const submitInput = (origValue) => {
        origValue = origValue.trim();
        if (origValue.length == 0 && defaultValue.length > 0) {
            origValue = defaultValue;
        }
        const { error, value } = schema.validate(origValue);
        if (error) {
            return setError(error.message);
        }
        onSubmit(value);
    };
    return (react_1.default.createElement(ink_1.Box, { flexDirection: 'column' },
        error
            ? (react_1.default.createElement(ink_1.Box, { borderStyle: "single", borderColor: "red" },
                react_1.default.createElement(ink_1.Text, { color: "red" }, error)))
            : null,
        react_1.default.createElement(ink_1.Box, { marginRight: 2 },
            react_1.default.createElement(ink_1.Text, null,
                title,
                defaultValue ? ` [${defaultValue}]` : '',
                ": "),
            react_1.default.createElement(ink_text_input_1.default, { mask: hidden ? '*' : void 0, value: text, onChange: setText, placeholder: placeholder, onSubmit: submitInput })),
        react_1.default.createElement(ink_1.Box, null,
            react_1.default.createElement(ink_1.Text, { color: "grey" }, helpText))));
};
exports.default = Setup;
