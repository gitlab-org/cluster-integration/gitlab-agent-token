"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(require("react"));
const ink_1 = require("ink");
const Summary = ({ gitlabUrl, projectPath, agentName, accessToken }) => {
    return (react_1.default.createElement(ink_1.Box, { flexDirection: "column", borderColor: "blue", borderStyle: "round", paddingX: 1 },
        react_1.default.createElement(ink_1.Box, { marginRight: 1 },
            react_1.default.createElement(ink_1.Text, null,
                "Your GitLab url is at: ",
                gitlabUrl)),
        react_1.default.createElement(ink_1.Box, { marginRight: 1 },
            react_1.default.createElement(ink_1.Text, null,
                "Private Access Token is ",
                accessToken ? react_1.default.createElement(ink_1.Text, { color: "green" }, "set") : react_1.default.createElement(ink_1.Text, { color: "yellow" }, "unset"))),
        react_1.default.createElement(ink_1.Box, { marginRight: 1 },
            react_1.default.createElement(ink_1.Text, null,
                "The project path is: ",
                projectPath)),
        react_1.default.createElement(ink_1.Box, { marginRight: 1 },
            react_1.default.createElement(ink_1.Text, null,
                "The agent name is: ",
                agentName))));
};
exports.default = Summary;
